import React from 'react';
import logo from './logo.svg';
import './App.css';

// Menggunakan Function
function App() {
	let nama = "Septy Almeria";
	let absen = 31;

	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<h1>Component Function</h1>
				<h2>Nama : {nama}</h2>
				<h2>No. Absen : {absen}</h2>
			</header>
		</div>
	);
}

// Menggunakan Variabel
const AppVar = () => {
  let nama = "Septy Almeria";
  let absen = 31;

  return (
  		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<h1>Component Variabel</h1>
				<h2>Nama : {nama}</h2>
				<h2>No. Absen : {absen}</h2>
			</header>
		</div>
    );
};

// Menggunakan Class
class AppClass extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      nama : "Septy Almeria",
      absen : 31, 
    };
  }

  render() {
    return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<h1>Component Class</h1>
				<h2>Nama : {this.state.nama}</h2>
				<h2>No. Absen : {this.state.absen}</h2>
			</header>
		</div>
      );
  }
}

export { App, AppVar, AppClass };
